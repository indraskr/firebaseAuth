import React, {useContext, useEffect, useState} from 'react';
import {auth} from '../firebase';

const AuthContext = React.createContext();

export function useAuth() {
  return useContext(AuthContext);
}

export function AuthProvider({children}) {
  const [currentUser, setCurrentUser] = useState();
  const [loading, setLoading] = useState(true);
  // Sign up
  function signup(email, password) {
    return auth.createUserWithEmailAndPassword(email, password);
  }

  // Login
  function login(email, password) {
    return auth.signInWithEmailAndPassword(email, password);
  }

  // Logout
  function logout() {
    return auth.signOut();
  }

  // Reset email
  function resetPassword(email) {
    return auth.sendPasswordResetEmail(email);
  }

  // send verification email
  function verifyUserEmail() {
    return auth.currentUser.sendEmailVerification();
  }

  // change avatar
  function changeAvatar(photoURL) {
    return auth.currentUser.updateProfile({photoURL});
  }

  // change name
  function changeName(displayName) {
    return auth.currentUser.updateProfile({displayName});
  }

  useEffect(() => {
    const unSub = auth.onAuthStateChanged(user => {
      setCurrentUser(user);
      setLoading(false);
    });
    return unSub;
  }, []);

  const value = {
    currentUser,
    signup,
    login,
    logout,
    resetPassword,
    verifyUserEmail,
    changeAvatar,
    changeName,
  };
  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  );
}
