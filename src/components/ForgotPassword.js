import {
  Alert,
  AlertIcon,
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  InputGroup,
  Text,
} from '@chakra-ui/core';
import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import {useAuth} from '../context/AuthContext';

const ForgotPassword = () => {
  const {resetPassword} = useAuth();
  const [email, setEmail] = useState('');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('');

  const onChange = e => setEmail(e.target.value);
  const onSubmit = async e => {
    e.preventDefault();
    try {
      setError('');
      setLoading(true);
      await resetPassword(email);
      setMessage('Reset link sent to your email');
    } catch (err) {
      setMessage('');
      setError(err.message);
    }
    setLoading(false);
  };

  return (
    <Flex minH='100vh' justify='center' align='center'>
      <Box
        w='full'
        maxW='450px'
        border='1px solid'
        borderColor='gray.300'
        borderRadius='5px'
        shadow='md'
        p='1rem 1.5rem'>
        <Heading textAlign='center' mb={5} as='h1' size='xl'>
          Reset Password
        </Heading>
        {error && (
          <Alert my={3} status='error'>
            <AlertIcon />
            {error}
          </Alert>
        )}
        {message && (
          <Alert my={3} status='success'>
            <AlertIcon />
            {message}
          </Alert>
        )}
        <form onSubmit={onSubmit}>
          <FormControl isRequired>
            <FormLabel color='gray.500' htmlFor='email'>
              Email
            </FormLabel>
            <InputGroup>
              <Input
                type='email'
                name='email'
                variant='outline'
                placeholder='Enter your email'
                value={email.trim()}
                onChange={onChange}
              />
            </InputGroup>
          </FormControl>
          <Button
            isLoading={loading}
            loadingText='Sending reset mail'
            mt={5}
            type='submit'
            variant='solid'
            variantColor='purple'>
            Reset
          </Button>
        </form>
        <Text fontSize='md' mt={3}>
          <Link to='/login'>Login</Link>
        </Text>
      </Box>
    </Flex>
  );
};

export default ForgotPassword;
