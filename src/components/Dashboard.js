import {
  Alert,
  AlertIcon,
  Avatar,
  Box,
  Button,
  ButtonGroup,
  Flex,
  Heading,
  Icon,
  Text,
  Tooltip,
  useToast,
} from '@chakra-ui/core';
import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import {useAuth} from '../context/AuthContext';
import {Link} from 'react-router-dom';

const Dashboard = () => {
  const {currentUser, logout, verifyUserEmail} = useAuth();
  const [error, setError] = useState('');
  const history = useHistory();
  const toast = useToast();

  function handleLogout() {
    try {
      logout();
      history.push('/login');
    } catch (err) {
      setError(err.message);
    }
  }

  console.log(currentUser);

  async function verifyEmail() {
    try {
      setError('');
      await verifyUserEmail();
      toast({
        description: 'Sent a mail with verification link.',
        status: 'success',
        duration: 9000,
        isClosable: true,
      });
    } catch (err) {
      setError(err.message);
    }
  }

  return (
    <Flex flexDir='column' minH='100vh' justify='center' align='center'>
      <Box
        pos='relative'
        // minH='25vh'
        w='full'
        maxW='450px'
        border='1px solid'
        borderColor='gray.300'
        p='1rem 1.2rem'
        shadow='md'>
        <Link to='/update-profile'>
          <Icon
            pos='absolute'
            top='10px'
            right='10px'
            name='settings'
            size='20px'
          />
        </Link>
        {error && (
          <Alert my={3} status='error'>
            <AlertIcon />
            {error}
          </Alert>
        )}
        <Avatar
          size='lg'
          name={currentUser.displayName}
          src={currentUser.photoURL}
        />
        <Heading as='h2' size='lg'>
          {currentUser.displayName || 'Username'}
        </Heading>
        <Text fontSize='xl'>
          Email: {currentUser.email}{' '}
          {!currentUser.emailVerified && (
            <Tooltip placeholder='right-end' label='Click to Verify Email'>
              <Icon
                onClick={verifyEmail}
                cursor='pointer'
                name='warning-2'
                mb={1}
                size='20px'
                color='red.500'
              />
            </Tooltip>
          )}
        </Text>

        <ButtonGroup mt={3}>
          <Button variant='solid'>Change Password</Button>
          <Button variant='solid'>Change Email</Button>
          <Button variant='solid' onClick={handleLogout}>
            Logout
          </Button>
        </ButtonGroup>
      </Box>
    </Flex>
  );
};

export default Dashboard;
