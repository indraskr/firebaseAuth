import {
  Alert,
  AlertIcon,
  Box,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  IconButton,
  Input,
  InputGroup,
  InputRightElement,
} from '@chakra-ui/core';
import React, {useRef, useState} from 'react';
import {useAuth} from '../context/AuthContext';

const UpdateProfile = () => {
  const {changeAvatar, changeName} = useAuth();
  const [error, setError] = useState('');
  const [message, setMessage] = useState('');
  const urlRef = useRef();
  const nameRef = useRef();

  async function onChangeAvatar() {
    try {
      await changeAvatar(urlRef.current.value);
      setMessage('Updated Profile pic');
    } catch (err) {
      setMessage('');
      setError(err.message);
    }
  }

  async function onNameChange() {
    try {
      await changeName(nameRef.current.value);
      setMessage('Updated name');
    } catch (err) {
      setMessage('');
      setError(err.message);
    }
  }

  return (
    <Flex minH='100vh' justify='center' align='center'>
      <Box
        w='full'
        maxW='450px'
        border='1px solid'
        borderColor='gray.300'
        borderRadius='5px'
        shadow='md'
        p='1rem 1.5rem'>
        <Heading textAlign='center' mb={5} as='h1' size='xl'>
          Update Profile
        </Heading>
        {error && (
          <Alert my={3} status='error'>
            <AlertIcon />
            {error}
          </Alert>
        )}
        {message && (
          <Alert my={3} status='success'>
            <AlertIcon />
            {message}
          </Alert>
        )}
        <form>
          <FormControl isRequired mt={2}>
            <FormLabel color='gray.500' htmlFor='avatar'>
              Avatar url
            </FormLabel>
            <InputGroup>
              <Input
                type='link'
                name='avatar'
                variant='outline'
                placeholder='Paste url'
                ref={urlRef}
                pr={10}
              />
              <InputRightElement>
                <IconButton
                  onClick={onChangeAvatar}
                  aria-label='Set avatar'
                  icon='check'
                />
              </InputRightElement>
            </InputGroup>
          </FormControl>
          <FormControl isRequired mt={2}>
            <FormLabel color='gray.500' htmlFor='name'>
              Update Name
            </FormLabel>
            <InputGroup>
              <Input
                type='text'
                name='name'
                variant='outline'
                placeholder='Enter your name'
                ref={nameRef}
                pr={10}
              />
              <InputRightElement>
                <IconButton
                  onClick={onNameChange}
                  aria-label='Change name'
                  icon='check'
                />
              </InputRightElement>
            </InputGroup>
          </FormControl>
        </form>
      </Box>
    </Flex>
  );
};

export default UpdateProfile;
