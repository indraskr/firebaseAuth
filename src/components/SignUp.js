import {
  Alert,
  AlertIcon,
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  InputGroup,
  InputRightElement,
  Text,
} from '@chakra-ui/core';
import React, {useState} from 'react';
import {Link, useHistory} from 'react-router-dom';

import {useAuth} from '../context/AuthContext';

const SignUp = () => {
  const {signup} = useAuth();
  const [show, setShow] = React.useState(false);
  const [user, setUser] = useState({
    email: '',
    password: '',
  });
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const {email, password} = user;
  const onChange = e => setUser({...user, [e.target.name]: e.target.value});
  const onSubmit = async e => {
    e.preventDefault();
    try {
      setError('');
      setLoading(true);
      await signup(email, password);
      history.push('/');
    } catch (err) {
      console.log(err);
      setError(err.message);
    }
    setLoading(false);
  };

  return (
    <Flex minH='100vh' justify='center' align='center'>
      <Box
        w='full'
        maxW='450px'
        border='1px solid'
        borderColor='gray.300'
        borderRadius='5px'
        shadow='md'
        p='1rem 1.5rem'>
        <Heading textAlign='center' mb={5} as='h1' size='xl'>
          Sign up
        </Heading>
        {error && (
          <Alert my={3} status='error'>
            <AlertIcon />
            {error}
          </Alert>
        )}
        <form onSubmit={onSubmit}>
          <FormControl isRequired>
            <FormLabel color='gray.500' htmlFor='email'>
              Email
            </FormLabel>
            <InputGroup>
              <Input
                type='email'
                name='email'
                variant='outline'
                placeholder='Enter your email'
                value={email.trim()}
                onChange={onChange}
              />
            </InputGroup>
          </FormControl>
          <FormControl isRequired mt={2}>
            <FormLabel color='gray.500' htmlFor='password'>
              Password
            </FormLabel>
            <InputGroup>
              <Input
                type={show ? 'text' : 'password'}
                name='password'
                variant='outline'
                value={password.trim()}
                placeholder='Password obviously'
                onChange={onChange}
              />
              <InputRightElement width='4.5rem'>
                <Button
                  h='1.75rem'
                  bg='gray.200'
                  size='sm'
                  onClick={() => setShow(!show)}>
                  {show ? 'Hide' : 'Show'}
                </Button>
              </InputRightElement>
            </InputGroup>
          </FormControl>
          <Button
            isLoading={loading}
            loadingText='Signing up'
            mt={5}
            type='submit'
            variant='solid'
            variantColor='purple'>
            Sign Up
          </Button>
        </form>
        <Text fontSize='md' mt={3}>
          Already have an account? <Link to='/login'>Login</Link>
        </Text>
      </Box>
    </Flex>
  );
};

export default SignUp;
